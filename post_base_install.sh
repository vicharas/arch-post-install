#!/bin/bash

USERNAME=alexandros
PASSWORD=none

# Add a user
useradd -m -g wheel -s /bin/bash $USERNAME
echo "$USERNAME:$PASSWORD" | chpasswd

PAC_PKG="unzip tmux vim xterm awesome chromium virtualbox-guest-utils xorg-server xorg-xinit virtualbox-guest-modules-arch bash-completion pastebinit wget git openssh" 

pacman -Sy $PAC_PKG --noconfirm

# Configure sudo
echo -e "root ALL=(ALL) ALL\n%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers

# Setup SSH
systemctl enable sshd.service
systemctl start sshd.service

# Configure xinitrc, openbox
runuser -l $USERNAME -c 'echo "exec awesome" > ~/.xinitrc'

# Setup pacaur
PACAUR_CMD="bash `pwd`/pacaur_install.sh"
runuser -l $USERNAME -c "$PACAUR_CMD"

# Configure git
git config --global user.email "alexandros.vichos@gmail.com"
git config --global user.name "Alexandros Vichos"

# tor browser
runuser -l $USERNAME -c "gpg --recv-keys D1483FA6C3C07136"
PACAUR_CMD="pacaur -S tor-browser-en --noconfirm --noedit"
runuser -l $USERNAME -c "$PACAUR_CMD"

